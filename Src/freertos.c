/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "moteur.h"
#include "gpio.h"
#include "usart.h"
#include "tim.h"
#include <string.h>
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId defaultTaskHandle;

/* USER CODE BEGIN Variables */
#define MAX_LEN 50

#define PID_KP 10
#define PID_KI 1/10
#define PID_KD 10

#define PID_PERIOD 1/100

uint8_t buffer[MAX_LEN];

int16_t target_droite = 0, target_gauche = 0;

/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
void testMoteur(TimerHandle_t timer);
void pidHandler(TimerHandle_t timer);
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  TimerHandle_t timer = xTimerCreate("testMoteur",
		  pdMS_TO_TICKS(5000),
		  pdTRUE,
		  NULL,
		  testMoteur);
  xTimerStart(timer, pdMS_TO_TICKS(100));

  TimerHandle_t acquisition = xTimerCreate("acquisition",
		  pdMS_TO_TICKS(10),
		  pdTRUE,
		  NULL,
		  pidHandler);
  xTimerStart(acquisition, pdMS_TO_TICKS(10));
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Application */
void testMoteur(TimerHandle_t timrt)
{
	static uint8_t sens=0;

	switch (sens)
	{
	// Moteur à l'arrêt
	case 0:
		sens=1;
		target_droite=330;
		target_gauche=-330;
		break;
	case 1:
		sens=0;
		target_droite=-330;
		target_gauche=330;
		break;
	}
}

void pidHandler(TimerHandle_t timer)
{
	static int32_t integ_droit = 0, integ_gauche=0;
	static int32_t last_err_droit = 0, last_err_gauche=0;

	int32_t err_droit, err_gauche;
	int32_t deriv_droit, deriv_gauche;
	int32_t cmd_droite, cmd_gauche;

	// Mesure de l'erreur
	err_droit = target_droite - motor_get_position(MOTEUR_DROITE);
	err_gauche = target_gauche - motor_get_position(MOTEUR_GAUCHE);

	// Integration de l'erreur
	integ_droit += (err_droit * PID_KI)*PID_PERIOD;
	integ_gauche += (err_gauche * PID_KI)*PID_PERIOD;

	// Derivation de l'erreur
	deriv_droit = (err_droit - last_err_droit) / PID_PERIOD;
	deriv_gauche = (err_gauche - last_err_gauche) / PID_PERIOD;

	last_err_droit = err_droit;
	last_err_gauche = err_gauche;

	// Calcul des commandes
	cmd_droite = PID_KP * err_droit + PID_KI * integ_droit + PID_KD * deriv_droit;
	cmd_gauche = PID_KP * err_gauche + PID_KI * integ_gauche + PID_KD * deriv_gauche;

	// Limite les commandes
	if (cmd_droite > 10000)
		cmd_droite = 10000;
	else if (cmd_droite < -10000)
		cmd_droite = -10000;

	if (cmd_gauche > 10000)
		cmd_gauche = 10000;
	else if (cmd_gauche < -10000)
		cmd_gauche = -10000;

	motor_command(MOTEUR_DROITE, -cmd_droite);
	motor_command(MOTEUR_GAUCHE, -cmd_gauche);
}

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
