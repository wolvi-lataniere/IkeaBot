/*
 *  IkeaBot, a simple robot for demonstration purpose
 *  Copyright (C) 2017    Aurelien VALADE
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 * moteur.c
 *
 *  Created on: 19 nov. 2017
 *      Author: Aurélien VALADE
 */

#include "moteur.h"
#include "tim.h"


/**
 * @brief Set the working point for the motor
 *
 * @param nom_moteur name (i.e. number) of the motor
 * @param speed motor speed to set : speed>0 : go forward
 * 									 speed<0 : go backward
 * 									 speed=0 : stop
 *  The motor speed is between -10000 and +10000
 */
void motor_command(MOTEUR nom_moteur, int16_t speed)
{
	// Compute the period and direction
	uint16_t active_period;
	uint8_t rotation_mode;

	// If the speed is positive
	if (speed >= 0)
	{
		// Compute the corresponding period from speed
		rotation_mode = 0; // Go forward
		active_period = (((uint32_t)speed)*MOTOR_TIMER_PERIOD)/ 10000;
	}
	else
	{
		// Compute the corresponding period from the absolute value of speed
		rotation_mode = 1; // Go backward
		active_period = (((uint32_t)-speed)*MOTOR_TIMER_PERIOD)/ 10000;
	}

	// Apply the changes to the motor driver
	switch (nom_moteur)
	{
	case MOTEUR_DROITE:
		if (rotation_mode == 0)
		{
			MOTOR_TIMER_HANDLER.Instance->MOTOR_1_NEG_CCR = 0;
			MOTOR_TIMER_HANDLER.Instance->MOTOR_1_POS_CCR = active_period;
			HAL_TIM_PWM_Start(&MOTOR_TIMER_HANDLER, MOTOR_1_POS_CHANNEL);
			HAL_TIM_PWM_Stop(&MOTOR_TIMER_HANDLER, MOTOR_1_NEG_CHANNEL);
		}
		else
		{
			MOTOR_TIMER_HANDLER.Instance->MOTOR_1_POS_CCR = 0;
			MOTOR_TIMER_HANDLER.Instance->MOTOR_1_NEG_CCR = active_period;
			HAL_TIM_PWM_Start(&MOTOR_TIMER_HANDLER, MOTOR_1_NEG_CHANNEL);
			HAL_TIM_PWM_Stop(&MOTOR_TIMER_HANDLER, MOTOR_1_POS_CHANNEL);
		}
		break;

	case MOTEUR_GAUCHE:
		if (rotation_mode == 0)
		{
			MOTOR_TIMER_HANDLER.Instance->MOTOR_2_NEG_CCR = 0;
			MOTOR_TIMER_HANDLER.Instance->MOTOR_2_POS_CCR = active_period;
			HAL_TIM_PWM_Start(&MOTOR_TIMER_HANDLER, MOTOR_2_POS_CHANNEL);
			HAL_TIM_PWM_Stop(&MOTOR_TIMER_HANDLER, MOTOR_2_NEG_CHANNEL);
		}
		else
		{
			MOTOR_TIMER_HANDLER.Instance->MOTOR_2_POS_CCR = 0;
			MOTOR_TIMER_HANDLER.Instance->MOTOR_2_NEG_CCR = active_period;
			HAL_TIM_PWM_Start(&MOTOR_TIMER_HANDLER, MOTOR_2_NEG_CHANNEL);
			HAL_TIM_PWM_Stop(&MOTOR_TIMER_HANDLER, MOTOR_2_POS_CHANNEL);
		}
		break;
	}


}



int16_t motor_get_position(MOTEUR nom_moteur)
{
	switch(nom_moteur)
	{
	case MOTEUR_DROITE:
		return MOTOR_1_ENC_TIMER_HANDLER.Instance->CNT;

	case MOTEUR_GAUCHE:
		return MOTOR_2_ENC_TIMER_HANDLER.Instance->CNT;
	}

	return 0;
}
