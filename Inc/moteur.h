/*
 *
 *  IkeaBot, a simple robot for demonstration purpose
 *  Copyright (C) 2017    Aurelien VALADE
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 * moteur.h
 *
 * Module de commande de moteurs pour IkeaBot... TODO :  Renommer le projet.
 *
 *  Created on: 19 nov. 2017
 *      Author: Aurélien VALADE
 */

#ifndef MOTEUR_H_
#define MOTEUR_H_

#include <stdint.h>

// Configuration definitions
#define MOTOR_TIMER_HANDLER htim2

#define MOTOR_TIMER_PERIOD 3200

#define MOTOR_1_POS_CHANNEL TIM_CHANNEL_1
#define MOTOR_1_NEG_CHANNEL TIM_CHANNEL_2
#define MOTOR_2_POS_CHANNEL TIM_CHANNEL_4
#define MOTOR_2_NEG_CHANNEL TIM_CHANNEL_3

#define MOTOR_1_POS_CCR CCR1
#define MOTOR_1_NEG_CCR CCR2
#define MOTOR_2_POS_CCR CCR4
#define MOTOR_2_NEG_CCR CCR3

#define MOTOR_1_ENC_TIMER_HANDLER htim3
#define MOTOR_2_ENC_TIMER_HANDLER htim4




// Typedef
typedef enum {
	MOTEUR_DROITE,
	MOTEUR_GAUCHE
}MOTEUR;

// Prototypes
void motor_command(MOTEUR nom_moteur, int16_t speed);
int16_t motor_get_position(MOTEUR nom_moteur);


#endif /* MOTEUR_H_ */
