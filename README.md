#  IkeaBot

A simple robot project for demonstration purpose.

# Description

This project is a simple STM32F103RB based robot firmware for a 2 motors robot.

# License

This project is designed by Aurelien VALADE and licensed under GPLv3.